class PhoneDetail
	require 'open-uri'
  # include ActiveModel::Model
  # attr_accessor :id, :name, :link

  # validates_presence_of :id, :name, :link

  def self.image_from_gsmarena(source)
	  source_base = "http://www.gsmarena.com/"
	  html_doc = Nokogiri::HTML(open(source))

    details_image = html_doc.css('div.specs-photo-main').to_html
    return details_image
  end

  def self.details_from_gsmarena(source)
	  source_base = "http://www.gsmarena.com/"
	  html_doc = Nokogiri::HTML(open(source))

    details = html_doc.css("#specs-list").to_html
    return details
  end


end