class Phone
	require 'open-uri'
  include ActiveModel::Model
  attr_accessor :id, :name, :link

  validates_presence_of :id, :name, :link

  def self.from_gsmarena(source)
  	# full name and link of the phone from http://www.gsmarena.com
	  source_base = "http://www.gsmarena.com/"
	  html_doc = Nokogiri::HTML(open(source))

	  rows = []
     html_doc.css('div.makers li').each_with_index do |row, i|
    	if row.css('a')[0].present?
    		phone = Phone.new(id: i + 1, name: row.text, link: source_base + row.css('a')[0]["href"])
		    rows << phone
		  end
    end
	  return rows
  end

end