class PhoneController < ApplicationController
  respond_to :html, :js

  def index
  	@brands = Brand.all_from_gsmarena
  end

  def product
    brands = Brand.all_from_gsmarena
    @source_brand = brands[params[:brand].to_i - 1].link
    @phones = Phone.from_gsmarena(@source_brand)
  end

  def product_details
    if params[:source].present?
  	  phones = Phone.from_gsmarena(params[:source])
    else
      phones = Search.from_gsmarena(params[:search_string])
    end
    source_detail = phones[params[:phone].to_i - 1].link
    @product_image = PhoneDetail.image_from_gsmarena(source_detail)
    @product_details = PhoneDetail.details_from_gsmarena(source_detail)
  end

  def search
    @phones = Search.from_gsmarena(params[:search_string])
    @search_string = params[:search_string]
  end
end
