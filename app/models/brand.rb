class Brand
	# brand name of the phone
	require 'open-uri'
  include ActiveModel::Model
  attr_accessor :id, :name, :link

  validates_presence_of :id, :name, :link

  def self.all_from_gsmarena 
    # all phone's brands from http://www.gsmarena.com
		source = "http://www.gsmarena.com/makers.php3"
	  source_base = "http://www.gsmarena.com/"
	  html_doc = Nokogiri::HTML(open(source))

	  rows = []
	  html_doc.xpath('//*/table/tr/td[2]').each_with_index do |row, i|
	  	if row.css('a')[0].present?        
        brand = Brand.new(id: i + 1, name: row.text, link: source_base + row.css('a')[0]["href"])
	      rows << brand
		  end
	  end
	  return rows
  end

end
